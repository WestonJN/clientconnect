import Head from 'next/head';
import PostCard from '../components/PostCard'
import Grid from '@material-ui/core/Grid'

export default function Content(){
    return(
        <Grid container spacing={1}>
            <Grid item xs={12}>
                <PostCard
                    date="27th July 2020"
                    title="How to manage a CRM"
                    author="Bob Synder"
                    text="Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                    when an unknown printer took a galley of type and scrambled it to make a type 
                    specimen book. It has survived not only five centuries, but also the leap into 
                    electronic typesetting, remaining essentially unchanged. It was popularised in
                    the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
                    and more recently with desktop publishing software like Aldus PageMaker including
                    versions of Lorem Ipsum"
                />
            </Grid>
            <Grid item xs={12}>
                <PostCard
                    date="20th July 2020"
                    title="Factors to consider when choosing a CRM"
                    author="John Deo"
                    text="Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                    when an unknown printer took a galley of type and scrambled it to make a type 
                    specimen book. It has survived not only five centuries, but also the leap into 
                    electronic typesetting, remaining essentially unchanged. It was popularised in
                    the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
                    and more recently with desktop publishing software like Aldus PageMaker including
                    versions of Lorem Ipsum"
                />
            </Grid>
            <Grid item xs={12}>
                <PostCard
                    date="01st July 2020"
                    title="4IR and how it will impact you"
                    author="Bob Synder"
                    text="Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                    when an unknown printer took a galley of type and scrambled it to make a type 
                    specimen book. It has survived not only five centuries, but also the leap into 
                    electronic typesetting, remaining essentially unchanged. It was popularised in
                    the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
                    and more recently with desktop publishing software like Aldus PageMaker including
                    versions of Lorem Ipsum"
                />
            </Grid>
           
        </Grid>
        
    )
}