import Head from 'next/head';
import { withStyles } from '@material-ui/core/styles';
import {Container} from "@material-ui/core";


export default function Footer() {
        return (
            <div>
                <Head>
                    <link href="../../static/style.css" rel="stylesheet" />
                </Head>
                <div >
                    <Container className="center-text">
                        <p className="dark-blue-text">Copyright © {new Date().getFullYear()}{'.'} A product of Turati Software </p>
                    </Container>
                </div>
            </div>
        )

    }


