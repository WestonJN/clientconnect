import Head from 'next/head';
import Link from 'next/link';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/styles';


const useStyles = makeStyles((theme) => ({
   
    body: {
      textAlign: 'justify',
      alignSelf: 'center',
    },
    media: {
        height: '200px'
    }
  }));

export default function ContactUs() {
    const classes = useStyles();

    return (
       
                <Grid container spacing={2}>
                    <Grid item xm={12} md={12} lg={12} className="contact-us-grid">
                        <Typography component="h1" variant="h3" className="custom-text-centered">
                            Contact Us
                        </Typography>
                        <Grid className={classes.body}>
                        
                            <p className="contact-us-form-text custom-mb-15">
                                Please fill in the contact form to get in touch with us!
                            </p>
                            <form noValidate autoComplete="off">
                                <div>
                                    <TextField id="contact-name-input" label="Todd Stevens"  />
                                </div>
                                <div>
                                    <TextField id="contact-email-input" label="someone@email.com"  />
                                </div>
                                <div>
                                    <TextField id="contact-message-input" label="Message" />
                                </div>
                            </form>
                            <p className="contact-us-form-text custom-mb-15">
                                Book Office, 20 Earth Street, Johannesburg, 2021 <br />
                                +27 (0)11 234 5678
                            </p>
                        
                        </Grid>
                        <div>
                            <iframe
                                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3581.9982519007467!2d28.061011315028562!3d-26.131599983470267!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1e950ce050106cdf%3A0x69f0da610d500bf2!2sTurati%20Software!5e0!3m2!1sen!2sza!4v1594389340691!5m2!1sen!2sza"
                                style={{ border: "0", width: "100%", height: "250" }}
                            ></iframe>
                        </div>
                    </Grid>
                </Grid>
                
    )
}
