import Head from 'next/head';
import PostCard from '../components/PostCard'
import Grid from '@material-ui/core/Grid'
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Card from '@material-ui/core/Card';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import Link from 'next/link';

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
      textAlign: 'center',
    },
    heading: {
      textAlign: 'center',
      display: 'block',
      color: '#3c3435',
    },
  
    body: {
      textAlign: 'justify',
      
    },
    media: {
        height: '200px'
    }
  }));


export default function Content(){
    const classes = useStyles();

    return(
       
        <Grid container spacing={2}>
        <Grid item xs={12}>
         
          </Grid>
         
          <Grid item xs={12}>
            <Typography className={classes.heading} >
               <h2>Who are we?</h2> 
            </Typography>
          </Grid>
          <Grid item xs={12} lg={4}>
          <CardMedia
          className={classes.media}
          image="/static/LogoNoBG.png"
          title="Client Connect"
            />
          </Grid>
          <Grid item xs={12} lg={8}>
          
              <Typography className={classes.body}>
                 <p>Client Connect is a sub-division of Turati 
                 Software which offers Technological solutions 
                 to all your business needs</p>

                 <p>With Client Connect we have managed to develop
                 a Customer Relationship Management system that is 
                 aimed at making the lives of all our customers easier.</p>

                 <p>We have focused on the complex components that businesses
                 struggle to manage and introduced a simple yet effiecient system
                 that requires very little training and expertise.</p>

                 <p>Click here to Get Started</p>
                
                    <Link href='../gettingStarted'>
                    <Button variant="outlined" color="primary">
                    Get Started
                    </Button>
                    </Link>
                
              </Typography>
          
          </Grid>
          <Grid item xs={12}>
            <Typography className={classes.heading}>
                <h2>Our Team</h2>
            </Typography>
          </Grid>
          <Grid item xs={12} lg={8}>
          
              <Typography className={classes.body}>
                  <p>We have a Dedicated team behind our CRM system that
                  aims at providing the best quality system at affordable
                  rates.</p>

                  <p>We cater largely for small businesses who would like 
                  to step up their game and improve the way their business
                  runs. </p>
                  <p>Our goal is to ensure that all businesses whether large 
                  or small have acces to quality systems at great prices.</p>
              </Typography>
          
          </Grid>
          <Grid item xs={12} lg={4}>
          <CardMedia
          className={classes.media}
          image="/static/techteam.jpg"
          title="Team"
            />
          
          </Grid>
        </Grid>          
        
    )
}