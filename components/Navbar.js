import { fade, makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import MoreIcon from '@material-ui/icons/MoreVert';
  import Head from 'next/head';
import Link from 'next/link'
import Router from 'next/router'
import Slide from '@material-ui/core/Slide';
import useScrollTrigger from '@material-ui/core/useScrollTrigger';
import PropTypes from 'prop-types';

const useStyles = makeStyles(theme => ({
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
 
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
  
}));

function HideOnScroll(props) {
  const { children, window } = props;
  // Note that you normally won't need to set the window ref as useScrollTrigger
  // will default to window.
  // This is only being set here because the demo is in an iframe.
  const trigger = useScrollTrigger({ target: window ? window() : undefined });

  return (
    <Slide appear={false} direction="down" in={!trigger}>
      {children}
    </Slide>
  );
}

HideOnScroll.propTypes = {
  children: PropTypes.element.isRequired,
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window: PropTypes.func,
};

export default function AppBarMCC() {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null);
  
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);


  function handleMobileMenuClose() {
    setMobileMoreAnchorEl(null);
  }
  
  function handleMobileMenuOpen(event) {
    setMobileMoreAnchorEl(event.currentTarget);
  }

  const mobileMenuId = 'primary-search-account-menu-mobile';
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
        <Link href='/index'>
            <MenuItem >
            <Typography>Home</Typography>
            </MenuItem>
        </Link>

        <Link href='../about'>      
            <MenuItem>
                <Typography>About Us</Typography>
            </MenuItem>
        </Link>

        <Link href='../pricing'>
            <MenuItem >
                <Typography> Pricing </Typography>
            </MenuItem>
        </Link>

        {/* <Link href='../demo'>
            <MenuItem >
                <Typography> Demo </Typography>
            </MenuItem>
        </Link> */}

        <Link href='../blog'>
            <MenuItem >
                <Typography> Blog </Typography>
            </MenuItem>
        </Link>
       
        {/* <Link href='../gettingStarted'>
            <MenuItem >
                <Typography> Getting Started </Typography>
            </MenuItem>
        </Link>  */}

        <Link href='../contact'>
            <MenuItem >
                    <Typography>Contact Us</Typography>
            </MenuItem>
        </Link>
    </Menu>
  );

  return (
    <div className={classes.grow}>
      <Head>
          <link href="../static/style.css" rel="stylesheet"/>
      </Head>
     
      <HideOnScroll>
      <AppBar color='default'>
        <Toolbar >
        <Link href="/index">
            <IconButton color="inherit">
                <img className="img-logo-mobile" alt="logo"  src="static/LogoNoBG.png" />
            </IconButton>
        </Link>
          <div className={classes.grow} />
          <div className={classes.sectionDesktop}>
            <Link href="/index">
                <IconButton  color="inherit">            
                    <Typography> Home </Typography>
                </IconButton>
            </Link>
            <Link href="../about">
                <IconButton color="inherit">
                    <Typography> About Us </Typography>
                </IconButton>
            </Link>
            <Link href="../pricing">
                <IconButton color="inherit">
                    <Typography> Pricing </Typography>
                </IconButton>
            </Link>
            {/* <Link href='../demo'>
            <MenuItem >
                <Typography> Demo </Typography>
            </MenuItem>
        </Link> */}
            <Link href="../blog">
                <IconButton color="inherit">
                    <Typography> Blog </Typography>
                </IconButton>
            </Link>
            {/* <Link href='../gettingStarted'>
            <MenuItem >
                <Typography> Getting Started </Typography>
            </MenuItem>
        </Link>  */}
            <Link href="../contact"> 
            <MenuItem href="../contact">
                    Contact Us
            </MenuItem>
            </Link>
          </div>
          <div className={classes.sectionMobile}>
            <IconButton
              aria-label="show more"
              aria-controls={mobileMenuId}
              aria-haspopup="true"
              onClick={handleMobileMenuOpen}
              color="inherit"
            >
              <MoreIcon />
            </IconButton>
          </div>
        </Toolbar>
      </AppBar>      
      </HideOnScroll>
      {renderMobileMenu}
      <Toolbar/>

      
    </div>
  );
}
