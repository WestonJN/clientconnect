import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Card from '@material-ui/core/Card';
import Typography from '@material-ui/core/Typography';
import Navbar from '../components/Navbar';
import Footer from '../components/footer';
import Button from '@material-ui/core/Button';
import Link from 'next/link';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    textAlign: 'center',
  },
  heading: {
    textAlign: 'center',
    display: 'block',
    color: '#3c3435',
  },

  body: {
    textAlign: 'center',
    color: '#3c3435',
  },

  media: {
    height:"500px"
  },
}));

export default function CenteredGrid() {
  const classes = useStyles();

  return (
    <div className={classes.container}>
      <Grid container direction="column" spacing={2}>
        <Grid item>
          <Navbar/>
        </Grid>
        <Grid item xs={12}>
            <Typography className={classes.heading}>
                <h1>Client Connect</h1>
                <h3> Connecting You To Your Customers</h3>
            </Typography>
        </Grid>
        <Grid item container>
          <Grid xs={false} sm={1}/>
          <Grid xs={12} sm={10}>
          <Card >
            <CardMedia
            className={classes.media}
            image="/static/crmhome.jpg"
            title="CRM Home"
              />
              <CardContent>
                <Typography className={classes.heading}gutterBottom variant="h5" component="h2">
                  Customer Relations made easy!
                </Typography>
                <Typography className={classes.body}variant="body2" color="textSecondary" component="p">
                 <p> Focus more on driving sales while Our CRM system makes it easier for
                  you to run your business. We understand that the relationship you have
                  with your customers is essential for your business to thrive. So we have
                  developed a simple and effecient system that allows you to manage your business.
                  </p>
                  <Link href='../demo'>
                    <Button variant="outlined" color="primary">
                    DEMO
                    </Button>
                    </Link>
                </Typography>
              </CardContent>

              </Card>
          </Grid>
          <Grid xs={false} sm={1}/>
        </Grid>

        <Grid item>
          <Footer/>
        </Grid>
      </Grid>
          
     
    </div>
  );
}
