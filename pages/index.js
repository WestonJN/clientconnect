import Head from 'next/head';
import Navbar from '../components/Navbar';
import Footer from '../components/footer'
import Home from '../components/Home';
import NextGa from 'next-ga';

export default function Index() {
  return (
    <div>
      <Head>
        <title>Client Connect</title>
        <link rel="icon" href="/favicon.ico" />
        <script
            async
            src="https://www.googletagmanager.com/gtag/js?id=UA-174143521-1"
          />

          <script
            dangerouslySetInnerHTML={{
              __html: `
                    window.dataLayer = window.dataLayer || [];
                    function gtag(){dataLayer.push(arguments);}
                    gtag('js', new Date());
                    gtag('config', 'UA-174143521-1');
                `,
            }}
          />
      </Head>
      
      <main>
      <Home/>
      </main>
    </div>
  )
}
