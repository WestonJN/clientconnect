import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Card from '@material-ui/core/Card';
import Typography from '@material-ui/core/Typography';
import  Navbar from '../components/Navbar'
import AboutUs from '../components/AboutUs'
import Footer from '../components/footer'

export default function CenteredGrid() {
  

  return (
    <div >
       <Grid container direction="column" spacing={2}>
        <Grid item>
          <Navbar/>
        </Grid>
      
        <Grid item container>
          <Grid xs={0} sm={1}/>
          <Grid xs={12} sm={10}>
            <AboutUs/>
          </Grid>
          <Grid xs={0} sm={1}/>
        </Grid>

        <Grid item>
          <Footer/>
        </Grid>
      </Grid>
      
    </div>
  );
}

 