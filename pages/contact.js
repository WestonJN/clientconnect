import Link from 'next/link'
import Navbar from '../components/Navbar'
import ContactUs from '../components/ContactUs'
import Footer from '../components/footer'
import Grid from '@material-ui/core/Grid'

export default function Contact() {
  return (
    
    <Grid container direction="column" spacing={2}>
    <Grid item>
      <Navbar/>
    </Grid>
  
    <Grid item container>
      <Grid xs={0} sm={1}/>
      <Grid xs={12} sm={10}>
        <ContactUs/>
      </Grid>
      <Grid xs={0} sm={1}/>
    </Grid>

    <Grid item>
      <Footer/>
    </Grid>
  </Grid>
    
  )
}