import Link from 'next/link'
import Navbar from '../components/Navbar'
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography'
import Footer from '../components/footer';
import Content from '../components/Content'

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  typography: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
}));

export default function Blog() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      
      <Grid container direction="column" spacing={2}>
        <Grid item>
          <Navbar/>
        </Grid>
      
        <Grid item container>
          <Grid xs={0} sm={1}/>
          <Grid xs={12} sm={10}>
            <Content/>
          </Grid>
          <Grid xs={0} sm={1}/>
        </Grid>

        <Grid item>
          <Footer/>
        </Grid>
      </Grid>
    </div>
  );
}
